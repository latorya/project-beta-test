from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Technician, Appointment, AutomobileVO
from common.json import ModelEncoder

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id"
        "vin",
        "sold",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "vin",
        "customer",
        "status",
        "technician",
        "id",
    ]
    encoders = {"technician": TechnicianEncoder(), "automobile": AutomobileVOEncoder}
    def get_extra_data(self, o):
        return {"technician": o.technician.first_name}



#****   FUNCTIONS FOR TECHNICIAN   ****

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method=="GET":
        technician = Technician.objects.all()
        return JsonResponse({"technician":technician}, encoder=TechnicianEncoder, safe=False)
    else:
        content=json.loads(request.body)  #get the technician object and load it in the content dictionary
        technician = Technician.objects.create(**content)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder,
            safe=False)


@require_http_methods(["DELETE"])
def api_delete_technician(request, id):
    if request.method=="DELETE":
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician does not exist"})


#****   FUNCTIONS FOR APPOINTMENTS   ****

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method=="GET":
        appointments = Appointment.objects.all()
        return JsonResponse({"appointments":appointments}, encoder=AppointmentEncoder, safe=False)
    else:
        try:
            content=json.loads(request.body)
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
            content["status"] = "created"
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
        )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Unable to create appointment"},
                status=400,
            )


@require_http_methods(["DELETE"])
def api_delete_appointment(request, pk):
    if request.method=="DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    if request.method=="PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "canceled"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Unable to change appointment"},
                status=400,
            )


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    if request.method=="PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.status = "finished"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Unable to create appointment"},
                status=400,
            )
