# CarCar

Team:

* Person 1 - LaTorya Walker, Automobile Sales, Inventory
* Person 2 - Silva Galstyan, Automobile Service, Inventory

## Overview
CarCar is an application designed to help manage a car dealership. It handles everything from managing the cars in stock, to organizing service appointments, and tracking sales.

## What's Inside?
CarCar is made up of different parts that work together:

- Inventory: Keeps track of all the cars available for sale.
- Service Center: Helps schedule and manage service appointments.
- Sales: Manages sales information.

# How to Run this App
Here's how you can run CarCar:
1. Install the following on your local machine:Docker, Git and Node.js
2. Fork then Clone the Repository: git clone <https://gitlab.com/latorya/project-beta.git>
3. Navigate to the Project Directory: cd CarCar
4. Start the Database Service:
    - docker volume create beta-data
    Build the Docker containers:
    - docker-compose build
    Run the application:
    - docker-compose up
5. Access the Application: You can find CarCar at http://localhost:3000 in your web browser.
   -![view website design](image.png)

## Design
![view project design](image-25.png)


## Service microservice

URLS & Ports:
INVENTORY:

Action	                       Method	        URL
List manufacturers	            GET           http://localhost:8100/api/manufacturers/
Create a manufacturer	        POST	      http://localhost:8100/api/manufacturers/
Get a specific manufacturer	    GET	          http://localhost:8100/api/manufacturers/:id/
Update a specific manufacturer	PUT	          http://localhost:8100/api/manufacturers/:id/
Delete a specific manufacturer	DELETE	      http://localhost:8100/api/manufacturers/:id/
Create an  Automobile           POST          http://localhost:8100/api/automobiles/  [View Here](image-22.png)
List Automobiles                GET           http://localhost:8100/api/automobiles/  [View Here](image-23.png)
Create a Vehicle Model          POST          http://localhost:8100/api/models/       [View Here](image-24.png)

SERVICES:

Action	                                Method	    URL
List technicians	                    GET	        http://localhost:8080/api/technicians/
Create a technician	                    POST	    http://localhost:8080/api/technicians/
Delete a specific technician	        DELETE	    http://localhost:8080/api/technicians/:id/
List appointments	                    GET	        http://localhost:8080/api/appointments/
Create an appointment	                POST	    http://localhost:8080/api/appointments/
Delete an appointment	                DELETE	    http://localhost:8080/api/appointments/:id/
Set appointment status to "canceled"	PUT	        http://localhost:8080/api/appointments/:id/cancel/
Set appointment status to "finished"	PUT	        http://localhost:8080/api/appointments/:id/finish/


## Sales microservice

The Sales service keeps track of car sales within the CarCar application. It includes information about salespeople, customers, and the details of each sale, such as the car sold, the salesperson involved, the customer, and the price.

When a car is sold, the Sales service checks with the Inventory service to make sure that the car is available for sale and hasn't already been sold. It ensures that only cars listed in the Inventory service can be sold.

For example, if someone tries to sell a car that isn't listed in the inventory or has already been sold, the Sales service won't allow it.

**note : only the cars available in the CarCar system can be sold (tracking who sold them and for how much).

## URLSs and Ports
Sales Service: http://localhost:8090/api/

Sales API
# List salespeople GET
http://localhost:8090/api/salespeople/ : It retrieves a list of all the sale records ![View Here](image-13.png)

# Create a salesperson	POST
http://localhost:8090/api/salespeople/ : It creates a new sales record ![View Here](image-14.png)

# Delete a specific salesperson	DELETE
http://localhost:8090/api/salespeople/:id/ : It allows you to delete a specific sales record using the ID ![View Here](image-15.png)

# List customers	GET
http://localhost:8090/api/customers/ : Displays a list of customers ![View Here](image-16.png)

# Create a customer	POST
http://localhost:8090/api/customers/ : Allows you to create a new customer ![View Here](image-17.png)

# Delete a specific customer	DELETE
http://localhost:8090/api/customers/:id/ : Allows you the option to delete a specific customer using their ID ![View Here](image-18.png)

# List sales	GET
http://localhost:8090/api/sales/ : Displays a list that includes all of the sales records ![View Here](image-19.png)

# Create a sale	POST
http://localhost:8090/api/sales/ : You can create a new sales record ![View Here](image-21.png)

# Delete a sale	DELETE
http://localhost:8090/api/sales/:id/ : Allows you the option to delete a specific sales record using the ID ![View Here](image-20.png)


Sales- Frontend
# Create a Model
http://localhost:3000/create-vehicle-model: Create a Model ![view here](image-3.png)
# List Automobiles
http://localhost:3000/list-automobiles: List automobiles ![view here](image-4.png)
# Create Automobiles
http://localhost:3000/create-automobiles: Create an automobile ![View Here](image-5.png)
# List Sales People
http://localhost:3000/list-salespeople: List Salespeople ![View Here](image-6.png)
# Add a Salesperson
http://localhost:3000/add-salesperson : Create a Salesperson ![View Here](image-7.png)
# Customers
http://localhost:3000/list-customers: List customers ![View Here](image-8.png)
# Add a Customer
http://localhost:3000/add-customer: Add a customer ![View Here](image-9.png)
# Sales
http://localhost:3000/list-sales: List of Sales ![View Here](image-10.png)
# Add a Sale
http://localhost:3000/record-sales: Record a new sale: ![View Here](image-11.png)
# Sales History
http://localhost:3000/list-salesperson-history: Sales history, filter by salesperson ![View Here](image-12.png)
### VALUE OBJECTS

Sales
Fields:
1. automobile (foreign key)
2. salesperson (foreign key)
3. customer (foreign key)
4. price
