import React, { useState, useEffect } from 'react';

function ListSales() {
  const [sales, setSales] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    async function fetchSales() {
      try {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
          const data = await response.json();
          setSales(data.sales || []);
        } else {
          console.error('Failed to fetch sales', response.statusText);
          setError('Failed to fetch sales: ' + response.statusText);
        }
      } catch (error) {
        console.error('Failed to fetch sales', error);
        setError('Failed to fetch sales: ' + error.message);
      }
    }
    fetchSales();
  }, []);

  return (
    <div>
      <h1>Sales</h1>
      {error && <p className="text-danger">{error}</p>}
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Employee ID</th>
            <th>Customer</th>
            <th>Automobile VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.length === 0 ? (
            <tr>
              <td colSpan="5">No sales found</td>
            </tr>
          ) : (
            sales.map((sale, index) => (
              <tr key={`${sale.id}_${index}`}>
                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                <td>{sale.salesperson.employee_id}</td>
                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            ))
          )}
        </tbody>
      </table>
    </div>
  );
}

export default ListSales;
