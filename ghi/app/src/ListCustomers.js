import React, { useState, useEffect } from 'react';

function ListCustomers() {
  const [customers, setCustomers] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    async function fetchCustomers() {
      try {
        const response = await fetch("http://localhost:8090/api/customers/");
        if (response.ok) {
          const data = await response.json();
          console.log("Fetched Customers:", data.customers);
          setCustomers(data.customers || []);
        } else {
          console.error("Failed to fetch customers", response.statusText);
          setError("Failed to fetch customers: " + response.statusText);
        }
      } catch (error) {
        console.error("Failed to fetch customers", error);
        setError("Failed to fetch customers: " + error.message);
      }
    }
    fetchCustomers();
  }, []);

  return (
    <div>
      <h1>Customers</h1>
      {error && <p>{error}</p>}
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customers.length === 0 ? (
            <tr>
              <td colSpan="4">No customers found</td>
            </tr>
          ) : (
            customers.map((customer, index) => (
              <tr key={index}>
                <td>{customer.first_name}</td>
                <td>{customer.last_name}</td>
                <td>{customer.phone_number}</td>
                <td>{customer.address}</td>
              </tr>
            ))
          )}
        </tbody>
      </table>
    </div>
  );
}

export default ListCustomers;
