import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddSalespersonForm from './AddSalespersonForm'
import ListSalesperson from './ListSalesperson';
import CustomerForm from './CustomerForm';
import ListCustomers from './ListCustomers';
import RecordSaleForm from './RecordSaleForm';
import ListSales from './ListSales';
import SalespersonHistoryList from './SalespersonHistoryList';
import CreateVehicleModelForm from './CreateVehicleModelForm';
import VehicleModelList from './VehicleModelList';
import AutomobileList from './AutomobileList';
import CreateAutomobileForm from './CreateAutomobileForm';
import ManufacturerForm from "./ManufacturerForm";
import ManufacturerList from "./ManufacturerList";
import TechnicianForm from "./TechnicianForm";
import TechnicianList from "./TechnicianList";
import AppointmentForm from "./AppointmentForm";
import AppointmentList from "./AppointmentList";
import AppointmentHistory from "./AppointmentHistory";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/add-salesperson" element={<AddSalespersonForm />} />
          <Route path="/list-salespeople" element={<ListSalesperson />} />
          <Route path="/add-customer" element={<CustomerForm />} />
          <Route path="/list-customers" element={<ListCustomers />} />
          <Route path="/record-sales" element={<RecordSaleForm />} />
          <Route path="/list-sales" element={<ListSales />} />
          <Route path="/list-salesperson-history" element={<SalespersonHistoryList />} />
          <Route path="/create-vehicle-model" element={<CreateVehicleModelForm />} />
          <Route path="models/" element={<VehicleModelList />} />
          <Route path="/list-automobiles" element={<AutomobileList />} />
          <Route path="/create-automobiles" element={<CreateAutomobileForm />} />
          <Route path="manufacturers/" element={<ManufacturerList />} />
          <Route path="manufacturers/create" element={<ManufacturerForm />} />
          <Route path="technicians/" element={<TechnicianList />} />
          <Route path="technicians/create" element={<TechnicianForm />} />
          <Route path="appointments/" element={<AppointmentList />} />
          <Route path="appointments/create" element={<AppointmentForm />} />
          <Route path="appointments/history" element={<AppointmentHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
