import React, { useState, useEffect } from 'react';

function RecordSaleForm() {
  const [formData, setFormData] = useState({
    automobile: '',
    salesperson: '',
    customer: '',
    price: ''
  });
  const [message, setMessage] = useState('');
  const [error, setError] = useState('');
  const [automobiles, setAutomobiles] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [salesperson, setSalesperson] = useState([]);

  useEffect(() => {
    const fetchAutomobiles = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
          const data = await response.json();
          const unsoldAutomobiles = data.automobiles.filter(automobile => !automobile.sold);
          setAutomobiles(unsoldAutomobiles);
        } else {
          throw new Error('Error fetching automobiles');
        }
      } catch (error) {
        console.error('Error:', error);
        setError('An error occurred while fetching automobiles');
      }
    };

    const fetchCustomers = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
          const data = await response.json();
          setCustomers(data.customers);
        } else {
          throw new Error('Error fetching customers');
        }
      } catch (error) {
        console.error('Error:', error);
        setError('An error occurred while fetching customers');
      }
    };

    const fetchSalesperson = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
          const data = await response.json();
          setSalesperson(data.salesperson || []);
        } else {
          throw new Error('Error fetching salespeople');
        }
      } catch (error) {
        console.error('Error:', error);
        setError('An error occurred while fetching salespeople');
      }
    };

    fetchAutomobiles();
    fetchCustomers();
    fetchSalesperson();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('http://localhost:8090/api/sales/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      });
      if (response.ok) {
        setMessage('Sale recorded successfully!');
        setFormData({
          automobile: '',
          salesperson: '',
          customer: '',
          price: ''
        });
      } else {
        let errorText = 'An error occurred while recording the sale.';
        if (response.status === 500) {
          errorText = 'Internal Server Error. Please try again later.';
        } else {
          const errorData = await response.json();
          if (errorData.error) {
            errorText = `Error: ${errorData.error}`;
          }
        }
        setError(errorText);
      }
    } catch (error) {
      console.error('Error:', error);
      setError('An error occurred. Please try again.');
    }
  };


  return (
    <div>
      <h1>Record a New Sale</h1>
      {error && <p className="text-danger">{error}</p>}
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="automobile">Automobile:</label>
          <select
            id="automobile"
            name="automobile"
            value={formData.automobile}
            onChange={handleChange}
            required
          >
            <option key="default" value="">Select Automobile</option>
            {automobiles.map((automobile) => (
              <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
            ))}
          </select>
        </div>
        <div>
          <label htmlFor="salesperson">Salesperson:</label>
          <select
            id="salesperson"
            name="salesperson"
            value={formData.salesperson}
            onChange={handleChange}
            required
          >
            <option key="default" value="">Select Salesperson</option>
            {salesperson.map((person) => (
              <option key={person.id} value={person.id}>{`${person.first_name} ${person.last_name}`}</option>
            ))}
          </select>
        </div>
        <div>
          <label htmlFor="customer">Customer:</label>
          <select
            id="customer"
            name="customer"
            value={formData.customer}
            onChange={handleChange}
            required
          >
            <option key="default" value="">Select Customer</option>
            {customers.map((customer) => (
              <option key={customer.id} value={customer.id}>{`${customer.first_name} ${customer.last_name}`}</option>
            ))}
          </select>
        </div>
        <div>
          <label htmlFor="price">Price:</label>
          <input
            type="text"
            id="price"
            name="price"
            value={formData.price}
            onChange={handleChange}
            required
            autoComplete="off"
          />
        </div>
        <button type="submit">Record Sale</button>
      </form>
      {message && <p>{message}</p>}
    </div>
  );
}

export default RecordSaleForm;




















