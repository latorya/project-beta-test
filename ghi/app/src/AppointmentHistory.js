import { useEffect, useState } from "react";

function AppointmentHistory() {
    const [appointments, setAppointments] = useState([]);
    const [search, setSearch] = useState("");

    const getData = async function () { //I can also do async () =>
        const response = await fetch('http://localhost:8080/api/appointments/');
        const autoResponse = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok && autoResponse.ok) {
            const data = await response.json();
            const appointmentList = data.appointments;
            const autoData = await autoResponse.json();
            const autoList = autoData.automobiles;
            const autoVins = autoList.map(autoData => autoData.vin)
            for (let appointment of appointmentList) {
                if (autoVins.includes(appointment.vin)) {
                    appointment["vip"] = "Yes";
                } else {
                    appointment["vip"] = "No";
                }
                setAppointments(appointmentList);
            }
        }
    }
        useEffect(() => {
            getData();
        }, []);

        function handleSearches(e) {
            setSearch(e.target.value)
        }
         const searchResults= appointments.filter(appointment =>
            appointment.vin.includes(search)
        );

  return (
    <>
    <h1>Service History</h1>
    <div className="input-group my-3">
        <input
        icon="search"
        onChange={handleSearches}
        type="search"
        placeholder="Search by VIN"
        className="form-control"/>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date & Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {searchResults.map(appointment => (
            <tr key={appointment.id}>
            <td>{ appointment.vin }</td>
            <td>{ appointment.vip }</td>
            <td>{ appointment.customer }</td>
            <td>{ appointment.date_time }</td>
            <td>{ appointment.technician }</td>
            <td>{ appointment.reason }</td>
            <td>{ appointment.status }</td>
            </tr>
        )
        )}
      </tbody>
    </table>
    </>
  );
}

export default AppointmentHistory;
